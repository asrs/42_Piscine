/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 15:31:45 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/25 11:29:59 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cat.h"

void			ft_putchar(char c)
{
	write(1, &c, 1);
}

void			ft_putstr(char *str)
{
	int			i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int				ft_strlen(char *str)
{
	int			i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void			ft_display(char *file, int select)
{
	int			fd;
	int			size;
	char		buffer[1024 + 1];
	if (select == 0)
		fd = open(file, O_RDONLY);
	else
		fd = 0;
	if (fd != -1)
	{
		while ((size = read(fd, buffer, 1024)) != 0)
		{
			buffer[size] = '\0';
			ft_putstr(buffer);
		}
		if (select == 0)
			close(fd);
	}
	else
	{
		write(1, "cat: ", 5);
		write(1, file, ft_strlen(file));
		write(1, ": No such file or directory\n", 28); 
	}
}

int				main(int ac, char **av)
{
	int			i;

	i = 1;
	if (ac == 1)
	{	
		ft_display(0, 1);
	}
	else if (ac > 1)	
	{
		while (i < ac)
		{
			ft_display(av[i], 0);
			i++;
		}
	}
	return (0);
}
