/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tail.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/23 19:17:14 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/25 15:13:30 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TAIL_H
# define TAIL_H

# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <fcntl.h>
# include <errno.h>

void		ft_putchar(char c);
void		ft_putstr(char *str);
void		ft_cend(char *str);
void		ft_csize(char *str, int n);
void		ft_cfront(char *str, int n);
void		ft_caller_one(int ac, char **av);
void		ft_caller_two(int ac, char **av);
void		ft_multiplay_one(int ac, char **av, int mode);
void		ft_multiplay_two(int ac, char **av, int mode);
void		ft_error(char *file);
void		ft_reader(char *file, int mode, int len, int msize);
char		*ft_strcpy(char *dest, char *src);
char		*ft_strcat(char *dest, char *src);
char		*ft_strdup(char *src);
char		*ft_realloc(char *print, char *buffer, int n);
int			ft_strlen(char *str);
int			ft_strcmp(char *s1, char *s2);
int			ft_is_alpha(char *str);
int			ft_atoi(char *str);

#endif
