/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nxt_two.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/24 13:54:34 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/25 11:39:41 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tail.h"

int					ft_strcmp(char *s1, char *s2)
{
	int				i;
	int				result;

	i = 0;
	result = 0;
	while (s1[i] == s2[i] && s1[i])
		i++;
	result = s1[i] - s2[i];
	return (result);
}

char				*ft_strdup(char *src)
{
	int				i;
	int				len;
	char			*dest;

	len = ft_strlen(src);
	if (!(dest = (char *)malloc(sizeof(char) * (len + 1))))
		return (0);
	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int					ft_atoi(char *str)
{
	int				k;

	k = 0;
	while (*str <= 32)
		str++;
	if (*str == '-' || *str == '+')
		str++;
	while ((*str >= '0' && *str <= '9') && *str)
	{
		k = (k * 10) + ((int)(*str) - '0');
		str++;
	}
	return (k);
}

int					ft_is_alpha(char *str)
{
	int				i;

	i = 0;
	while (str[i])
	{
		if ((str[i] >= 'a' && str[i] <= 'z') ||
					(str[i] >= 'A' && str[i] <= 'Z'))
			i++;
		else
			return (0);
	}
	return (1);
}

void				ft_error(char *file)
{
	ft_putstr(ft_strdup("tail : "));
	ft_putstr(ft_strdup(file));
	ft_putstr(ft_strdup(": No such file or directory\n"));
}
