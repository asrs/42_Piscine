/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/23 16:58:12 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/25 15:16:55 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tail.h"
#include <stdio.h>

void			ft_multiplay_one(int ac, char **av, int mode)
{
	int			i;
	int			fd;

	i = 1;
	while (i < ac)
	{
		fd = open(av[i], O_RDONLY);
		if (av[i] != 0 && fd != -1)
		{
			ft_putstr(ft_strdup("==> "));
			ft_putstr(av[i]);
			ft_putstr(ft_strdup(" <==\n"));
			ft_reader(av[i], mode, ft_atoi(av[2]), 0);
			ft_putstr(ft_strdup("\n"));
		}
		else
			ft_error(av[i]);
		close(fd);
		i++;
	}
}

void			ft_multiplay_two(int ac, char **av, int mode)
{
	int			i;
	int			fd;

	i = 3;
	while (i < ac)
	{
		fd = open(av[i], O_RDONLY);
		if (av[i] != 0 && fd != -1)
		{
			ft_putstr(ft_strdup("==> "));
			ft_putstr(av[i]);
			ft_putstr(ft_strdup(" <==\n"));
			ft_reader(av[i], mode, ft_atoi(av[2]), 0);
			ft_putstr(ft_strdup("\n"));
		}
		else
			ft_error(av[i]);
		close(fd);
		i++;
	}
}

char			*ft_realloc(char *print, char *buffer, int n)
{
	char		*dest;

	dest = (char *)malloc(sizeof(char) * n);
	dest = ft_strcat(print, buffer);
	return (dest);
}

void			ft_reader(char *file, int mode, int len, int msize)
{
	int			fd;
	int			size;
	char		buffer[1024 + 1];
	char		*print;

	fd = open(file, O_RDONLY);
	print = (char *)malloc(sizeof(char) * 1024);
	if (fd != -1)
	{
		while ((size = read(fd, buffer, 1024)) != 0)
		{
			msize += size;
			buffer[size] = '\0';
			print = ft_realloc(print, buffer, msize);
		}
		close(fd);
		if (mode == 0)
			ft_cend(print);
		else if (mode == 1)
			ft_cfront(print, len);
		else if (mode == 2)
			ft_csize(print, len);
	}
	else
		ft_error(file);
}

int				main(int ac, char **av)
{
	if (ac < 2)
		return (0);
	else if (ft_strcmp(av[1], ft_strdup("-c")) == 0)
		ft_caller_one(ac, av);
	else if (ft_strcmp(av[1], ft_strdup("-c")) != 0)
	{
		if (ac == 2)
		{
			ft_reader(av[1], 0, 0, 0);
		}
		else if (ac >= 3)
		{
			ft_multiplay_one(ac, av, 0);
		}
	}
	return (0);
}
