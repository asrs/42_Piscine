/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/15 18:03:12 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/19 17:20:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (i + 1);
}

int			ft_arglen(int argc, char **argv, int total)
{
	int		i;

	i = 0;
	while (i < argc)
	{
		total += ft_strlen(argv[i]);
		i++;
	}
	return (total);
}

char		*ft_concat_params(int argc, char **argv)
{
	char	*final;
	int		i;
	int		j;
	int		k;
	int		total;

	i = 1;
	k = 0;
	total = ft_arglen(argc, argv, 0);
	final = (char *)malloc(sizeof(char) * total);
	while (i < (argc))
	{
		j = 0;
		while (k < total && argv[i][j])
		{
			final[k] = argv[i][j];
			k++;
			j++;
		}
		final[k] = (i < (argc - 1)) ? '\n' : '\0';
		k++;
		i++;
	}
	return (final);
}
