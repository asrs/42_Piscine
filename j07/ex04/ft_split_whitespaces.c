/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/18 15:37:59 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/19 10:21:13 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stddef.h>

char			*ft_ptr(char *str, int n)
{
	int			i;
	int			ptr;

	i = 0;
	ptr = 0;
	while (str[i] && ptr < n)
	{
		if ((str[i - 1] == ' ' || str[i - 1] == '\t' || str[i - 1] == '\n' ||
					str[i - 1] == '\0') && str[i] > 32)
			ptr++;
		i++;
	}
	return (&str[i - 1]);
}

int				ft_end(char *str)
{
	int			i;

	i = 0;
	while (str[i] != ' ' && str[i] != '\t' && str[i] != '\n' && str[i])
		i++;
	return (i);
}

void			ft_wrd(char *str, int *wrd)
{
	int			i;

	i = 0;
	while (str[i])
	{
		if ((str[i - 1] == ' ' || str[i - 1] == '\t' || str[i - 1] == '\n' ||
					str[i - 1] == '\0') && str[i] > 32)
			(*wrd)++;
		i++;
	}
}

char			**ft_split_whitespaces(char *str)
{
	char		**final;
	int			wrd;
	int			i;

	wrd = 0;
	ft_wrd(str, &wrd);
	if (!(final = (char **)malloc(sizeof(char *) * (wrd + 1))))
		return (0);
	i = 0;
	while (i < wrd)
	{
		final[i] = ft_ptr(str, (i + 1));
		i++;
	}
	i = 0;
	while (i < wrd)
	{
		final[i][ft_end(final[i])] = '\0';
		i++;
	}
	final[i] = NULL;
	return (final);
}
