/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/10 22:00:46 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/13 16:32:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char			*ft_strstr(char *str, char *to_find)
{
	int			i;
	int			j;
	int			size;

	size = 0;
	while (to_find[size])
		size++;
	i = 0;
	while (str[i])
	{
		if (str[i] == to_find[0])
		{
			j = 0;
			while (str[i + j] == to_find[0 + j])
			{
				j++;
			}
			if (j >= size)
				return (&str[i]);
		}
		i++;
	}
	return (0);
}
