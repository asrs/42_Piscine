/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   doop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 17:51:42 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/22 16:51:43 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DOOP_H
# define DOOP_H

# include <unistd.h>
# include <stddef.h>

void		ft_putchar(char c);
void		ft_pustr(char *str);
void		ft_putnbr(long int nb);
int			ft_atoi(char *str);
int			ft_get_value(char *str);

#endif
