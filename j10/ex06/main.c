/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 17:54:46 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/22 17:04:55 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"
#include <stdio.h>

int			ft_get_sign(char *str)
{
	if (str[0] == '+')
		return (1);
	else if (str[0] == '-')
		return (2);
	else if (str[0] == '*')
		return (3);
	else if (str[0] == '/')
		return (4);
	else if (str[0] == '%')
		return (5);
	else
		return (0);
	return (0);
}

int			ft_get_value(char *str)
{
	int		i;
	int		value;

	i = 0;
	value = 0;
	while (str[i])
	{
		value = ft_atoi(str);
		i++;
	}
	return (value);
}

int			ft_calculate(int n1, int n2, int op, int *jump)
{
	int		final;

	final = 0;
	if (op == 1)
		final = n1 + n2;
	else if (op == 2)
		final = n1 - n2;
	else if (op == 3 && n2 != 0)
		final = n1 * n2;
	else if (op == 4 && n2 != 0)
		final = n1 / n2;
	else if (op == 5 && n2 != 0)
		final = n1 % n2;
	else if (op == 4 && n2 == 0)
	{
		write(1, "Stop : division by zero\n", 24);
		(*jump)++;
	}
	else if (op == 5 && n2 == 0)
	{
		write(1, "Stop : modulo by zero\n", 22);
		(*jump)++;
	}
	return (final);
}

int			main(int ac, char **av)
{
	int		op1;
	int		op2;
	int		math;
	int		act;

	op1 = 0;
	op2 = 0;
	if (ac != 4)
		return (0);
	if (av[1] == NULL || av[2] == NULL || av[3] == NULL)
	{
		ft_putchar('0');
		ft_putchar('\n');
		return (0);
	}
	op1 = ft_get_value(av[1]);
	op2 = ft_get_value(av[3]);
	math = ft_calculate(op1, op2, ft_get_sign(av[2]), &act);
	if (act == 0)
	{
		ft_putnbr(math);
		ft_putchar('\n');
	}
	return (0);
}
