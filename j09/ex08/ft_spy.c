/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_spy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 11:00:05 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/14 11:02:13 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int			ft_strstr(char *s1, char *s2)
{
	int		i;
	int		k;
	int		j;

	i = 0;
	while (s1[i])
	{
		j = 0;
		k = i;
		while (s2[j] == s1[k])
		{
			j++;
			k++;
			if (s2[j] == 0)
				return (1);
		}
		i++;
	}
	return (0);
}

void		all_lower(int argc, char **argv)
{
	int		i;
	int		j;

	i = 1;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j])
		{
			if (argv[i][j] >= 'A' && argv[i][j] <= 'Z')
				argv[i][j] += 32;
			j++;
		}
		i++;
	}
}

int			ft_strcmp(char *s1, char *s2)
{
	while (*s1 && *s2 && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

int			main(int argc, char **argv)
{
	int		i;

	i = 1;
	all_lower(argc, argv);
	while (i < argc)
	{
		if (ft_strstr(argv[i], "president") == 1 ||
				ft_strstr(argv[i], "attack") == 1 ||
				ft_strstr(argv[i], "powers") == 1)
			write(1, "Alert!!!\n", 9);
		i++;
	}
	return (0);
}
