/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/13 17:46:50 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/13 21:27:32 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>

void		ft_takes_place(int hour)
{
	if (hour == 0)
	{
		write(1, "THE FOLLOWING TAKES PLACE BETWEEN ", 35);
		printf("%d.00 P.M. AND %d.00 A.M", hour, hour + 1);
	}
	if (hour == 24)
	{
		write(1, "THE FOLLOWING TAKES PLACE BETWEEN ", 35);
		printf("%d.00 P.M. AND %d.00 A.M", hour - 12, hour - 23);
	}
	if (hour == 12)
	{
		write(1, "THE FOLLOWING TAKES PLACE BETWEEN ", 35);
		printf("%d.00 A.M. AND %d.00 P.M", hour, hour - 11);
	}
	if (hour < 12 && hour > 0)
	{
		write(1, "THE FOLLOWING TAKES PLACE BETWEEN ", 35);
		printf("%d.00 A.M. AND %d.00 A.M", hour, hour + 1);
	}
	if (hour >= 13 && hour < 24)
	{
		write(1, "THE FOLLOWING TAKES PLACE BETWEEN ", 35);
		printf("%d.00 P.M. AND %d.00 P.M", hour - 12, hour - 11);
	}
}
