/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 12:03:41 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/22 16:42:17 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVAL_H
# define EVAL_H

int		eval_expr(char *str);
int		calc_add(int nbr, char signe, char *str);
int		calc_mul(int nbr, char signe, char *str);

#endif
