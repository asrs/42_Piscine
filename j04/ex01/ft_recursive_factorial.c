/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/09 10:50:17 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/11 00:25:19 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_recursive_factorial(int nb)
{
	int			result;

	if (nb == 0 || nb == 1)
		return (1);
	else if (nb >= 2 && nb < 13)
		result = (nb * ft_recursive_factorial(nb - 1));
	else
		return (0);
	return (result);
}
