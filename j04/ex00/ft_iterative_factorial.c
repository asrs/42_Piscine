/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/09 10:14:57 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/11 00:22:09 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			calculate(int nb, int i, int temp)
{
	while (i <= nb)
	{
		if (temp > 0)
			nb *= temp;
		i++;
		temp--;
	}
	return (nb);
}

int			ft_iterative_factorial(int nb)
{
	int			result;

	if (nb == 0 || nb == 1)
		return (1);
	else if (nb >= 2 && nb < 13)
		result = calculate(nb, 0, (nb - 1));
	else
		return (0);
	return (result);
}
