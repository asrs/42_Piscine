/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/06 04:40:48 by clrichar          #+#    #+#             */
/*   Updated: 2017/07/07 13:19:22 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_putnbr(int nb)
{
	unsigned int		nbr;

	nbr = nb;
	if (nb < 0)
	{
		ft_putchar('-');
		nbr = nbr * -1;
	}
	if (nbr >= 10)
	{
		ft_putnbr(nbr / 10);
		ft_putnbr(nbr % 10);
	}
	else
	{
		ft_putchar(nb + '0');
	}
}

void		ft_display(int i1, int i2)
{
	if (i1 <= 9)
	{
		ft_putnbr(0);
	}
	ft_putnbr(i1);
	ft_putchar(' ');
	if (i2 <= 9)
	{
		ft_putnbr(0);
	}
	ft_putnbr(i2);
	if (i1 != 98)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void		ft_print_comb2(void)
{
	signed int		i1;
	signed int		i2;

	i1 = 0;
	while (i1 <= 99)
	{
		i2 = i1 + 1;
		while (i2 <= 99)
		{
			ft_display(i1, i2);
			i2++;
		}
		i1++;
	}
}
