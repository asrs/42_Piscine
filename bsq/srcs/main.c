/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/18 20:30:42 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/26 13:57:41 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "bsq.h"

char				*read_file(int fd, int *s_total, char *path)
{
	char			buff[SIZE + 1];
	int				size;
	char			*str;

	if (fd != 0)
		return (opti_read(fd, s_total, path));
	*s_total = 0;
	str = NULL;
	while ((size = read(fd, buff, SIZE)) > 0)
	{
		buff[size] = 0;
		*s_total += size;
		str = ft_realloc(str, *s_total);
		ft_strcat(str, buff);
	}
	if (size == -1)
		return (NULL);
	return (str);
}

char				*get(char *tmp, int i, t_type *type)
{
	type->empty = tmp[i];
	i++;
	while (tmp[i] && tmp[i] != '\n' && (tmp[i] == ' ' || tmp[i] == '\t'))
		i++;
	if (tmp[i] == 0)
		return (NULL);
	type->ob = tmp[i];
	i++;
	while (tmp[i] && tmp[i] != '\n' && (tmp[i] == ' ' || tmp[i] == '\t'))
		i++;
	if (tmp[i] == 0)
		return (NULL);
	type->full = tmp[i];
	i++;
	while (tmp[i] && tmp[i] != '\n' && (tmp[i] == ' ' || tmp[i] == '\t'))
		i++;
	if (tmp[i] == '\n')
		return (&tmp[i + 1]);
	return (NULL);
}

t_type				*get_type(char **str)
{
	t_type			*type;
	char			*tmp;
	int				i;

	if ((type = malloc(sizeof(t_type))) == NULL)
		return (NULL);
	tmp = *str;
	i = 0;
	type->size = ft_atoi(tmp);
	if (type->size == 0)
		return (NULL);
	while (tmp[i] >= '0' && tmp[i] <= '9')
		i++;
	if ((*str = get(tmp, i, type)) == NULL)
		return (NULL);
	return (type);
}

bool				read_all(int fd, char *path)
{
	t_type			*type;
	t_square		square;
	char			*str;
	int				*tab;
	int				s_total;

	square.size = 0;
	square.x = 0;
	if ((str = read_file(fd, &s_total, path)) == NULL)
		return (false);
	square.str = str;
	if ((type = get_type(&str)) == NULL)
		return (false);
	if ((tab = (int *)malloc(sizeof(int) * s_total)) == NULL)
		return (false);
	if (algo(str, tab, type, &square) == false)
		return (false);
	free(type);
	free(tab);
	free(square.str);
	return (true);
}

int					main(int argc, char **argv)
{
	int				fd;
	int				i;

	i = 1;
	if (argc == 1 && read_all(0, NULL) == false)
	{
		write(2, "Error\n", 6);
		return (1);
	}
	else
	{
		while (i < argc)
		{
			if ((fd = open(argv[i], O_RDONLY)) < 0 || read_all(fd, argv[i]) == false)
			{
				write(2, "map error\n", 10);
				return (1);
			}
			close(fd);
			i++;
			if (argc > 2 && i < argc)
				write(1, "\n", 1);
		}
	}
	return (0);
}
