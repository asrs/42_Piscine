/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/24 17:45:57 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/24 17:49:57 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void			ft_memset(void *data, char value, size_t size)
{
	char		*mem;

	mem = (char *)data;
	while (mem < (char *)data + size)
	{
		*mem = value;
		mem++;
	}
}
