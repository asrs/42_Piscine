/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erwberna <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 16:42:16 by erwberna          #+#    #+#             */
/*   Updated: 2017/07/26 12:41:45 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdbool.h>
#include <unistd.h>
#include "bsq.h"

void		print_res(char *str, t_square *square, t_map *map_info, int size)
{
	int		x;
	int		y;
	int		l_size;
	int		nb_ligne;

	l_size = map_info->l_size;
	nb_ligne = map_info->nb_ligne;
	x = square->x / l_size;
	y = square->x % l_size;
	while (x >= 0 && x >= (int)(square->x / l_size - square->size) + 1)
	{
		y = square->x % l_size;
		while (y >= 0 && y >= (int)(square->x % l_size - square->size) + 1)
		{
			str[x * (l_size + 1) + y] = 'x';
			y--;
		}
		x--;
	}
	write(1, str, size);
}

bool		check_map(char str, t_map *map_info)
{
	if (str == '\n')
	{
		if (map_info->l_size == 0)
			map_info->l_size = map_info->ligne;
		if (map_info->nb_ligne != 0 && map_info->l_size != map_info->ligne)
			return (false);
		map_info->nb_ligne = map_info->nb_ligne + 1;
		map_info->ligne = 0;
	}
	else
		map_info->ligne = map_info->ligne + 1;
	return (true);
}

void		getsize_atpos(int *tab, int j, t_map *map_info, t_square *square)
{
	if (map_info->nb_ligne == 0)
		tab[j] = 1;
	else if (j % map_info->l_size == 0)
		tab[j] = 1;
	else if (tab[j - 1] <= tab[j - map_info->l_size] &&
			tab[j - 1] < tab[j - map_info->l_size - 1])
		tab[j] = tab[j - 1] + 1;
	else if (tab[j - map_info->l_size] < tab[j - 1] &&
			tab[j - map_info->l_size] < tab[j - map_info->l_size - 1])
		tab[j] = tab[j - map_info->l_size] + 1;
	else
		tab[j] = tab[j - map_info->l_size - 1] + 1;
	if (square->size < tab[j])
	{
		square->x = j;
		square->size = tab[j];
	}
}

bool		algo(char *str, int *tab, t_type *chara, t_square *square)
{
	size_t	j;
	size_t	i;
	t_map	map_info;

	j = 0;
	i = 0;
	ft_memset(&map_info, 0, sizeof(map_info));
	while (str[i] &&
			(str[i] == chara->ob || str[i] == chara->empty || str[i] == '\n'))
	{
		if (str[i] == chara->ob)
			tab[j] = 0;
		else if (str[i] == chara->empty)
			getsize_atpos(tab, j, &map_info, square);
		if (check_map(str[i], &map_info) == false)
			return (false);
		if (str[i] != '\n')
			j++;
		i++;
	}
	if (map_info.nb_ligne != chara->size || map_info.l_size == 0)
		return (false);
	print_res(str, square, &map_info, i);
	return (true);
}
